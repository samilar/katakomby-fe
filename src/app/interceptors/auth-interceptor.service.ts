import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  // HttpResponse
} from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { catchError, finalize, from, Observable, of, tap, throwError } from "rxjs";
import {
  TOKEN_HEADER_KEY,
  TOKEN_TYPE
} from "../services/mock.service";
import { AuthService } from "../services/auth.service";
import { LoadingService } from "../services/loading.service";
// import { fromPromise } from "rxjs/internal-compatibility";
// import { mergeMap, switchMap } from "rxjs/operators";

export const localApiUrl = 'http://localhost:4200';
/*
export function countXpForNextLevel(actualLevel: number) {
  return actualLevel * PER_LEVEL_XP_REQUIREMENT;
}

export const isUsed = (roomIndex: number, used: Enemy[]) => {
  return used.filter((enemy: Enemy) => enemy.position === roomIndex ).length !== 0;
};
export const getRoomIndex = (min: number, max: number, player: Player, used?: Enemy[]) => {
  min = Math.ceil(1);
  max = Math.floor(max);
  let roomIndex = Math.floor(Math.random() * (max - min + 1)) + min;
  if (used && used.length > 0 && isUsed(roomIndex, used) || player?.position === roomIndex) {
    roomIndex = getRoomIndex(min, max, player, used);
  }
  return roomIndex;
};
*/
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private timer?: number;

  constructor(
    private injector: Injector,
    private authService: AuthService,
    private loadingService: LoadingService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.timer) {
      this.loadingService.loadingStart();
    }
    request = AuthInterceptor.addTokenHeader(request, this.authService.getGapi().currentUser.get().getAuthResponse().access_token);

    // wierd! This need to be here to get changes in headers immediately after addTokenHeaders() or it will appear only in lazyUpdate request headers property
    // request.headers.get(TOKEN_HEADER_KEY);

    return next.handle(request)
      .pipe(
        catchError<HttpEvent<any>, Observable<HttpEvent<any>>>((error: any): any => {
          if (
            error instanceof HttpErrorResponse
            && error.status === 401
          ) {
            this.handle401ErrorByRefreshingTokenAndResendRequest(request, next, error);
          }
          return throwError(error);
        }),
        finalize(() => {
          if (this.timer) {
            clearTimeout(this.timer);
          }
          this.timer = setTimeout(() => {
            this.loadingService.loadingStop();
            delete this.timer;
          }, 150);
        })
      );
  }

  private handle401ErrorByRefreshingTokenAndResendRequest(request: HttpRequest<any>, next: HttpHandler, error: HttpErrorResponse): Observable<any>|HttpRequest<any> {
    let refreshReq = request;
    this.authService.getGapi().currentUser.get().reloadAuthResponse()
      .then(() => {
        refreshReq = AuthInterceptor.addTokenHeader(request, this.authService.getGapi().currentUser.get().getAuthResponse().access_token)
      });
    return refreshReq;
  }

  private static addTokenHeader(request: HttpRequest<any>, token: string | null): HttpRequest<any> {
    return request.clone({headers: request.headers.set(TOKEN_HEADER_KEY, TOKEN_TYPE + ' ' + token)});
  }

}
