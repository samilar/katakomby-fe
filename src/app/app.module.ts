import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FloorComponent } from "./components/floor/floor.component";
import { PlayerAndEnemyService } from "./services/player-and-enemy.service";
import { PlayerControlsComponent } from "./components/player-controls/player-controls.component";
import { FightComponent } from "./components/fight/fight.component";
import { MinimapComponent } from "./components/minimap/minimap.component";
import { MinimapRoomComponent } from "./components/minimap-room/minimap-room.component";
import { MinimapPlayerComponent } from "./components/minimap-player/minimap-player.component";
import { MinimapEnemyComponent } from "./components/minimap-enemy/minimap-enemy.component";
import { NewLevelComponent } from "./components/new-level/new-level.component";
import { MovementsLayerComponent } from "./components/movements-layer/movements-layer.component";
import { FloorDetailsComponent } from "./components/floor-details/floor-details.component";
import { FightEntityBoxComponent } from "./components/fight-entity-box/fight-entity-box.component";
import { MockService } from "./services/mock.service";
import { FightLogComponent } from "./components/fight-log/fight-log.component";
import { FightResultsComponent } from "./components/fight-results/fight-results.component";
import { RoomViewComponent } from "./components/room-view/room-view.component";
import {
  HTTP_INTERCEPTORS,
  HttpClientModule
} from "@angular/common/http";
import { AuthInterceptor } from "./interceptors/auth-interceptor.service";
import { LoginComponent } from "./components/login/login.component";
import { MinimapItemComponent } from "./components/minimap-item/minimap-item.component";
import { NewItemComponent } from "./components/new-item/new-item.component";
import { FightItemPickComponent } from "./components/fight-item-pick/fight-item-pick.component";
import { InventoryComponent } from "./components/inventory/inventory.component";
import { InventoryItemComponent } from "./components/inventory-item/inventory-item.component";
import { LoadingSpinnerComponent } from "./components/loading-spinner/loading-spinner.component";
import { GearItemComponent } from "./components/gear-item/gear-item.component";

@NgModule({
    declarations: [
        AppComponent,
        FloorComponent,
        MinimapRoomComponent,
        MinimapPlayerComponent,
        MinimapEnemyComponent,
        PlayerControlsComponent,
        FightComponent,
        MinimapComponent,
        MinimapRoomComponent,
        MinimapPlayerComponent,
        MinimapEnemyComponent,
        NewLevelComponent,
        MovementsLayerComponent,
        FloorDetailsComponent,
        FightEntityBoxComponent,
        FightLogComponent,
        FightResultsComponent,
        RoomViewComponent,
        LoginComponent,
        MinimapItemComponent,
        NewItemComponent,
        FightItemPickComponent,
        InventoryComponent,
        InventoryItemComponent,
        GearItemComponent,
        LoadingSpinnerComponent
    ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    PlayerAndEnemyService,
    MockService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
