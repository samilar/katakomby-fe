import { Injectable } from "@angular/core";
import { Room } from "../interfaces/room.interface";
import { Floor } from "../interfaces/floor.interface";

// temporary constants meant for random or BE configuration
export const FLOOR = 1;
export const FLOOR_ROWS = 4;
export const FLOOR_COLS = 5;
export const ALLOWED_LOSTS = 5;
export const ALLOWED_BLOCKATIONS = 5;
export const START_POSITION = '';
export const ENEMIES_PER_ROOM = 3;
export const ENTITIES_VISIBLE_ON_MINIMAP = false;
export const PER_LEVEL_XP_REQUIREMENT = 512;
export const PER_LEVEL_ATTRIBUTES_POINTS = 10;
export const LOCAL_STORAGE_KEY = 'katakomby-cache-player-' + '1';

// textures hard setting for room and floor
export const USED_TEXTURE_PACK = '1';
export const FLOOR_MINIMAP_BG = '3';
export const ROOM_VIEW_THEME = '0';

export const GOOGLE_CLIENT_ID = '22733392508-ardts29tihhchtsqp1j7a46hfe26qo0v.apps.googleusercontent.com';
export const TOKEN_HEADER_KEY = 'Authorization';
export const TOKEN_TYPE = 'Bearer';
export const ITEMS_DROP_RATE_PERCENTAGE = 10;

@Injectable({
  providedIn: 'root'
})

export class MockService {

  public static defaultFloor: Floor = {
    floor: FLOOR,
    minimapBackground: FLOOR_MINIMAP_BG,
    themePack: ROOM_VIEW_THEME,
    rows: FLOOR_ROWS,
    cols: FLOOR_COLS
  };

  public static roomMap: Room[] = [{
    room_id: '',
    roomIndex: 1,
    col: 0,
    row: 0,
    exitWest: true,
    exitEast: false,
    exitNorth: false,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 2,
    col: 1,
    row: 0,
    exitWest: true,
    exitEast: true,
    exitNorth: false,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 3,
    col: 2,
    row: 0,
    exitWest: true,
    exitEast: true,
    exitNorth: false,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 4,
    col: 3,
    row: 0,
    exitWest: true,
    exitEast: true,
    exitNorth: false,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 5,
    col: 4,
    row: 0,
    exitWest: false,
    exitEast: true,
    exitNorth: false,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 6,
    col: 0,
    row: 1,
    exitWest: true,
    exitEast: false,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 7,
    col: 1,
    row: 1,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 8,
    col: 2,
    row: 1,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 9,
    col: 3,
    row: 1,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 10,
    col: 4,
    row: 1,
    exitWest: false,
    exitEast: true,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 11,
    col: 0,
    row: 2,
    exitWest: true,
    exitEast: false,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 12,
    col: 1,
    row: 2,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 13,
    col: 2,
    row: 2,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 14,
    col: 3,
    row: 2,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 15,
    col: 4,
    row: 2,
    exitWest: false,
    exitEast: true,
    exitNorth: true,
    exitSouth: true,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 16,
    col: 0,
    row: 3,
    exitWest: true,
    exitEast: false,
    exitNorth: true,
    exitSouth: false,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 17,
    col: 1,
    row: 3,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: false,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 18,
    col: 2,
    row: 3,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: false,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 19,
    col: 3,
    row: 3,
    exitWest: true,
    exitEast: true,
    exitNorth: true,
    exitSouth: false,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }, {
    room_id: '',
    roomIndex: 20,
    col: 4,
    row: 3,
    exitWest: false,
    exitEast: true,
    exitNorth: true,
    exitSouth: false,
    revealed: false,
    texturePack: USED_TEXTURE_PACK,
    items: []
  }];

}
