import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { Room } from "../interfaces/room.interface";
import { Player } from "../interfaces/player.interface";
import { FightRemains, FightRemainsPlayer, FightRemainsEnemy } from "../interfaces/fight.interface";
import { Direction } from "../interfaces/types/direction.type";
import { Floor } from "../interfaces/floor.interface";
import { EntityStatisticsType } from "../interfaces/types/entityStatisticsType.type";
import { Enemy } from "../interfaces/enemy.interface";
import { EntityStatistics } from "../interfaces/types/entityStatistics.type";
import { HttpClient } from "@angular/common/http";
import { Config } from "../interfaces/config.interface";
import { AuthService } from "./auth.service";
import { Item } from "../interfaces/item.interface";
import { Gear } from "../interfaces/gear.interface";
import { Ability } from "../interfaces/ability.interface";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class PlayerAndEnemyService {

  config: BehaviorSubject<Config> = new BehaviorSubject<Config>({} as Config);

  public rooms: Room[] = [];
  floor: Floor = {} as Floor;

  player: BehaviorSubject<Player> = new BehaviorSubject<Player>({} as Player);

  possibleFight: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  activeFight: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  activeFightEnemy: BehaviorSubject<Enemy|null> = new BehaviorSubject<Enemy|null>(null);
  activeFightLog: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  enemyDefending: boolean = false;

  floorEnemies: Enemy[] = [];

  newLevelAttributePointsDistribution: EntityStatistics = {} as any;
  tempFreeAbilityPoints?: BehaviorSubject<number>;

  transferFightItemsFromFightItemPickComponent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private lastPlayerPosition: string = '';
  private activeFightEntitiesRemains: FightRemains = {} as FightRemains;
  private _canStartFight: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  private apiUrl: string = environment.production ? 'https://katakomby-express.ey.r.appspot.com' : 'http://localhost:8080';
  // private apiUrl: string = 'https://katakomby-express.ey.r.appspot.com';
  private newItems: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>([]);
  private fightItems: Item[] = [];
  private floorReset: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private allExplored: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private playerItemsGearAndAbilities?: Gear[];

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {
  }

  getAuth(): AuthService {
    return this.authService;
  }

  initApplication() {

    // Initially load and sets application floor number and floor size (rows/cols) for rendering
    this.loadFloor();


    // Loads config from API after player is loaded (need this call order!!)
    this.loadConfig();

    this.floorReset.subscribe((state: boolean) => {
      if (state) {
        location.reload();
      }
    });

    this.allExplored.subscribe((state: boolean) => {
      if (state) {
        this.resetFloor();
      }
    });

    this.activeFight.subscribe((state: boolean) => {
      if (state && (!this.activeFightEntitiesRemains.player || !this.activeFightEntitiesRemains.enemy)) {
        this.activeFightEnemy.next(this.getRoomEnemy(this.getActiveRoom()));
        this.possibleFight.next(true);
        this.doFight();
      }
    });

    // Subscribe for any changes on player object
    this.player.subscribe((player: Player) => {
      console.log('player changed to', player);
      // Check if room contains any enemy
      if (this.hasRoomEnemy(this.getActiveRoom())) {
        // Show fight or flee window
        this.startFightOrFlee(
          this.getRoomEnemy(
            this.getActiveRoom()
          )
        );
      }
      this.revealRoomOnPosition(this.getActiveRoom()?.roomIndex);

      if (
        !(
          this.isFightPossible()
          && (
            !this.isFightEntitiesPresent()
            && !this.isFightActivated()
          )
        )
      ) {
        // Store player actual data in cache
        // this.storePlayerToCache();
      }

      this.handleNewFreeAbilityPoints();
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  getFightRemains() {
    // player/getRemains
    this.httpClient.post<FightRemains>(this.apiUrl + "/player/getRemains", {
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId()
    }).subscribe((fightRemains: FightRemains) => {
      // console.log('/player/getRemains response', fightRemains);
      this.activeFightEntitiesRemains = fightRemains;
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  loadRoomsEnemies(): void {
    // if () {
    //
    // }
    this.httpClient.post<Enemy[]>(this.apiUrl + "/floorEnemies", {
      floor: this.floor.floor,
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId()
    }).subscribe( (res: Enemy[]) => {
      // console.log('/floorEnemies response', res);
      res = this.handleIncomingBooleansOFFloorEnemies(res);
      this.floorEnemies = res;

      this.isAllExplored();

      this.loadIsFightActivated();
      if (this.hasRoomEnemy(this.getActiveRoom())) {
        this.startFightOrFlee(
          this.getRoomEnemy(
            this.getActiveRoom()
          )
        );
      }
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  loadRoomOfFloor(floor: Floor): void {
    this.httpClient.post<Room[]>(this.apiUrl + "/rooms", {
      floor: floor.floor,
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId()
    }).subscribe( (res: Room[]) => {
      res.map((r: Room) => {
        r.exitWest = (r.exitWest).toString() === 'true';
        r.exitEast = (r.exitEast).toString() === 'true';
        r.exitNorth = (r.exitNorth).toString() === 'true';
        r.exitSouth = (r.exitSouth).toString() === 'true';
        r.revealed = (r.revealed).toString() === 'true';
        return r;
      });
      // console.log('/rooms response', res);
      this.rooms = res;
      // Load and sets rooms enemies
      this.loadRoomsEnemies();
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  loadFloor(): void {
    this.httpClient.post(this.apiUrl + "/floor", {
      floor: this.player.getValue().actualFloorNumber || 1
    }).subscribe( (res: any) => {
      // console.log('/floor response', res);
      this.floor = res;
      // Check/restore cache
      this.loadPlayer();
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  storePlayerLastPosition() {
    if (this.player.getValue() && this.getLastPosition() !== '') {
      this.httpClient.post(this.apiUrl + "/player/storeLastPosition", {
        playerId: this.player.getValue().player_id,
        userId: this.authService.getGapi().currentUser.get().getId(),
        position: this.getLastPosition()
      }).subscribe( (res: any) => {
        res = this.handleIncomingBooleansOFPlayer(res);
        // console.log('/player/storeLastPosition response', res);
        this.player.next(res);
      });
    } else {
      console.log('CANT STORE LAST POSITION! LAST POSITION IS UNKNOWN...');
    }
  }

  // @TODO: future request place
  // @TODO: DONE
  doFight() {
    this.transferFightItemsFromFightItemPickComponent.next(true);
    if (this.canStartFight()) {
      this.httpClient.post<FightRemains>(this.apiUrl + "/player/initFight", {
        playerId: this.player.getValue().player_id,
        userId: this.authService.getGapi().currentUser.get().getId(),
        enemyId: this.activeFightEnemy.getValue()?.id,
        fromPosition: this.getLastPosition(),
        usedItems: this.getFightItems()
      }).subscribe( (res: FightRemains) => {
        // console.log('/player/initFight response', res);
        this.activeFightEntitiesRemains = res;
        this.activeFight.next(true);
        this.transferFightItemsFromFightItemPickComponent.next(false);
      });
    }
  }

  // @TODO: future request place
  // @TODO: DONE
  endFight() {
    this.httpClient.post<{
      remains: FightRemains;
      fightLog: string[];
      // battleRound: number;
      // blockedBoth: number;
      floorEnemies: Enemy[];
      player: Player;
    }>(this.apiUrl + "/player/closeFight", {
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId()
    }).subscribe( (res: {
      remains: FightRemains;
      fightLog: string[];
      floorEnemies: Enemy[];
      player: Player;
    }) => {
      // console.log('/player/closeFight response', res);

      res.floorEnemies = this.handleIncomingBooleansOFFloorEnemies(res.floorEnemies);
      // this.activeFightEntitiesRemains = res.remains;
      this.activeFightEntitiesRemains = {} as FightRemains;

      this.activeFightLog.next(res.fightLog);

      res.player = this.handleIncomingBooleansOFPlayer(res.player);
      // console.log('/player/attackEnemy response', res);
      this.playerItemsGearAndAbilities = res.player.items;
      this.playerItemsGearAndAbilities = this.handleIncomingBooleansOFItem(this.playerItemsGearAndAbilities);
      this.player.next(res.player);

      this.filterAndSetOnlyItemTypeFromPlayerItems();
      this.filterAndSetOnlyUsedGearTypeFromPlayerItems();

      this.floorEnemies = res.floorEnemies;
      console.log('new this.floorEnemies', this.floorEnemies);
      // this.activeFight.next(res.player.activeFight)

      this.possibleFight.next(false);
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  attackEnemy(usedItem?: Item) {
    this.httpClient.post<{
      fightLog: string[];
      remains: FightRemains;
      // battleRound: number;
      // blockedBoth: number;
      player: Player;
      activeFight: boolean;
    }>(this.apiUrl + "/player/attackEnemy", {
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId(),
      enemyId: this.activeFightEnemy.getValue()?.id,
      usedItem: (usedItem ? usedItem : null)
    }).subscribe( (res: {
      fightLog: string[];
      remains: FightRemains;
      // battleRound: number;
      // blockedBoth: number;
      player: Player;
      activeFight: boolean;
    }) => {
      res.player = this.handleIncomingBooleansOFPlayer(res.player);
      // console.log('/player/attackEnemy response', res);
      this.playerItemsGearAndAbilities = res.player.items;
      this.playerItemsGearAndAbilities = this.handleIncomingBooleansOFItem(this.playerItemsGearAndAbilities);
      this.player.next(res.player);

      this.filterAndSetOnlyItemTypeFromPlayerItems();
      this.filterAndSetOnlyUsedGearTypeFromPlayerItems();

      this.activeFightEntitiesRemains = res.remains;
      this.activeFightLog.next(res.fightLog);
      // this.blockedBoth = res.blockedBoth;
      // this.battleRound = res.battleRound;
      this.activeFight.next(res.activeFight);
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  go(direction: Direction) {
    this.isAllExplored();
    this.loadIsFightActivated();
    this.setLastPosition();
    this.httpClient.post<{
                  player: Player,
                  rooms: Room[],
                  newItems: Item[]
                }>(this.apiUrl + "/player/go", {
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId(),
      direction: direction
    }).subscribe( (res: {
                  player: Player,
                  rooms: Room[],
                  newItems: Item[]
                }
    ) => {
      res.newItems = this.handleIncomingBooleansOFItem(res.newItems);
      res.rooms = this.handleIncomingBooleansOFRoom(res.rooms);
      res.player = this.handleIncomingBooleansOFPlayer(res.player);

      // console.log('/player/go response', res);

      // this.loadRoomsEnemies();

      this.rooms = res.rooms;
      this.newItems.next(res.newItems);
      this.playerItemsGearAndAbilities = res.player.items;
      this.playerItemsGearAndAbilities = this.handleIncomingBooleansOFItem(this.playerItemsGearAndAbilities);
      this.player.next(res.player);

      this.filterAndSetOnlyItemTypeFromPlayerItems();
      this.filterAndSetOnlyUsedGearTypeFromPlayerItems();
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  loadCanStartFight() {
    return this.httpClient.post<boolean>(this.apiUrl + "/player/canStartFight", {
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId()
    })
      .subscribe((res) => {
        // console.log('/player/canStartFight response', res);
        this._canStartFight.next(res)
      });
  }

  // @TODO: future request place
  // @TODO: DONE
  canStartFight() {
    return this._canStartFight.getValue()
  }

  // @TODO: future request place
  // @TODO: DONE
  fleeFight() {
    this.httpClient.post<Player>(this.apiUrl + "/player/fleeFight", {
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId()
    }).subscribe((player: Player) => {

      // Set fight possibility to default and unload enemy from cache
      this.markFightFleetAndUnloadEnemy();

      if (!this.canStartFight()) {
        this.resetFloor();
      }
      player = this.handleIncomingBooleansOFPlayer(player);
      this.playerItemsGearAndAbilities = player.items;
      this.playerItemsGearAndAbilities = this.handleIncomingBooleansOFItem(this.playerItemsGearAndAbilities);
      this.player.next(player);

      this.filterAndSetOnlyItemTypeFromPlayerItems();
      this.filterAndSetOnlyUsedGearTypeFromPlayerItems();
    });

  }

  // @TODO: future request place
  // @TODO: DONE
  loadPlayer() {
    this.httpClient.post<Player>(this.apiUrl + "/player", {
      playerId: this.player.getValue().player_id || 1,
      userId: this.authService.getGapi().currentUser.get().getId()
    }).subscribe( (res: Player) => {
      res = this.handleIncomingBooleansOFPlayer(res);
      // console.log('/player response', res);
      this.playerItemsGearAndAbilities = res.items;
      this.playerItemsGearAndAbilities = this.handleIncomingBooleansOFItem(this.playerItemsGearAndAbilities);
      this.player.next(res);

      this.filterAndSetOnlyItemTypeFromPlayerItems();
      this.filterAndSetOnlyUsedGearTypeFromPlayerItems();

      // Initially load and sets rooms of actual floor
      this.loadRoomOfFloor(this.floor);

      // Initially store last position for fleeing
      this.setLastPosition();
      this.handleNewFreeAbilityPoints();
    }, (err) => {
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  loadConfig() {
    this.httpClient.post<Config>(this.apiUrl + "/config", {
    }).subscribe( (res: Config) => {
      // console.log('/config response', res);
      this.config.next(res);
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  saveNewAttributes() {
    this.setPlayerNewEntityStatistics(this.newLevelAttributePointsDistribution)
    this.httpClient.post<Player>(this.apiUrl + "/player/newLevel", {
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId(),
      strength: this.newLevelAttributePointsDistribution.strength || 0,
      defence: this.newLevelAttributePointsDistribution.defence || 0,
      intelligence: this.newLevelAttributePointsDistribution.intelligence || 0,
      vitality: this.newLevelAttributePointsDistribution.vitality || 0,
      agility: this.newLevelAttributePointsDistribution.agility || 0
    }).subscribe((player: Player) => {
      this.clearNewLevelTempData();
      player = this.handleIncomingBooleansOFPlayer(player);
      this.player.next(player);
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  useGear(item: Gear) {
    this.httpClient.post<Player>(this.apiUrl + "/player/useGear", {
      player: this.player.getValue(),
      // playerId: this.player.getValue().player_id,
      // userId: this.authService.getGapi().currentUser.get().getId(),
      usedItem: item
    }).subscribe((player: Player) => {
      player = this.handleIncomingBooleansOFPlayer(player);
      // console.log('/player response', res);
      this.playerItemsGearAndAbilities = player.items;
      this.playerItemsGearAndAbilities = this.handleIncomingBooleansOFItem(this.playerItemsGearAndAbilities);

      this.filterAndSetOnlyItemTypeFromPlayerItems();
      this.filterAndSetOnlyUsedGearTypeFromPlayerItems();

      this.player.next(player);
    });
  }

  // @TODO: future request place
  // @TODO: DONE
  resetFloor() {
    let wantReset = confirm("Je tředa si odpočinout nebo už není co zkoumat! Chceš načíst nové patro nyní?");
    if (wantReset) {
      this.httpClient.post(this.apiUrl + "/floor/reset", {
        playerId: this.player.getValue().player_id,
        userId: this.authService.getGapi().currentUser.get().getId()
      }).subscribe(() => {
        this.floorReset.next(true);
      });
    }
  }

  // @TODO: future request place
  // @TODO: DONE
  loadIsFightActivated() {
    this.httpClient.post<boolean>(this.apiUrl + "/player/isFightActive", {
      playerId: this.player.getValue().player_id,
      userId: this.authService.getGapi().currentUser.get().getId()
    }).subscribe((isFightActive: boolean) => {
      isFightActive = (isFightActive).toString() === 'true';
      this.activeFight.next(isFightActive);
    });
  }

  // NOT REQUEST METHODS(with requests)

  getItemOrGearTitle(itemOrGear: Gear): string {
    switch (itemOrGear.type) {
      case "gear":
        return (
            itemOrGear.affectHow === 'positive' ? 'Přidá' : 'Ubere'
          ) + ' '
          + (
            itemOrGear.affectWho === 'player' ? 'hráči' : 'nepříteli'
          ) + ' '
          + (itemOrGear.affectHow === 'positive' ? '+' : '-')
          + itemOrGear.power
          + ' '
          + (
            itemOrGear.affectWhat === 'hitpoints' ? 'životů' : ''
          )
          + (
            itemOrGear.affectWhat === 'defence' ? 'obrany' : ''
          )
          + (
            itemOrGear.affectWhat === 'agility' ? 'obratnosti' : ''
          )
          + (
            itemOrGear.affectWhat === 'vitality' ? 'životaschopnosti' : ''
          )
          + (
            itemOrGear.affectWhat === 'intelligence' ? 'inteligence' : ''
          )
          + (
            itemOrGear.affectWhat === 'strength' ? 'síly' : ''
          )
          + ' k statistikám postavy'
      case "item":
      default:
        return (
            itemOrGear.affectHow === 'positive' ? 'Přidá' : 'Ubere'
          ) + ' '
          + (
            itemOrGear.affectWho === 'player' ? 'hráči' : 'nepříteli'
          ) + ' '
          + itemOrGear.power
          + ' '
          + (
            itemOrGear.affectWhat === 'hitpoints' ? 'životů' : ''
          )
          + ' běhěm ' + itemOrGear.duration + ' kol(a)';
    }
  }

  storePlayerToCache() {
    if (this.player.getValue()?.player_id) {
      localStorage.setItem(this.config.getValue()?.LOCAL_STORAGE_KEY, JSON.stringify(this.player.getValue()));
    }
  }

  filterAndSetOnlyItemTypeFromPlayerItems() {
    const player = this.player.getValue();
    player.items = [];
    player.items = (
        this.playerItemsGearAndAbilities?.filter((item: Item|Gear) => (item.type === "item" || (item.type === "gear" && (false === (item as Gear).wearing) || (null === (item as Gear).wearing)))) || []
      );
    this.player.next(player);
  }

  filterAndSetOnlyUsedGearTypeFromPlayerItems() {
    const player = this.player.getValue();
    player.gear = [];
    player.gear = (
        this.playerItemsGearAndAbilities?.filter((gear: Gear) => gear.type === "gear" && true === gear.wearing) || []
      );
    this.player.next(player);
  }

  haveSomeNewItems(): boolean {
    return this.newItems.getValue().length > 0;
  }

  getNewItems(): Item[] {
    return this.newItems.getValue();
  }

  deleteNewItems() {
    // let updated = this.player.getValue();
    this.newItems.next(
      this.newItems.getValue().filter((item: Item) => {
        // updated.items.push(item);
        return false;
      })
    );
    // updated = this.handleIncommingBooleansOFPlayer(updated);
    // this.player.next(updated);
  }

  getRoomBackground() {
    return 'room-view-texture-' + (this.getActiveRoom()?.texturePack || '0') + '-' + (this.floor?.themePack || '0');
  }

  setLastPosition() {
    this.loadCanStartFight();
    this.lastPlayerPosition = this.player.getValue().position;
  }

  clearLastPosition() {
    this.lastPlayerPosition = '';
  }

  getLastPosition() {
    return this.lastPlayerPosition || this.player.getValue()?.position;
  }

  getRoomsForRow(row: number): Room[] {
    return this.rooms.filter((room: Room) => room.row === row);
  }

  getRowsForRooms() {
    const ret: number[] = [];
    this.rooms.map((r: Room) => {
      if (ret && ret.indexOf(r.row) === -1) {
        ret.push(r.row);
      }
    });
    return ret;
  }

  isFightPossible(): boolean {
    return this.possibleFight.getValue();
  }

  isFightActivated(): boolean {
    return this.activeFight.getValue();
  }

  hasRoomEnemy(room: Room): boolean {
    return this.floorEnemies.filter((enemy: Enemy) => {
      return enemy.position === room?.room_id;
    }).length > 0;
  }

  getRoomEnemy(room: Room): Enemy {
    if (this.hasRoomEnemy(room)) {
      return this.floorEnemies.filter((enemy: Enemy) => enemy.position === room.room_id)[0];
    }
    return {} as Enemy;
  }

  startFightOrFlee(enemy: Enemy) {
    // Show enemy on minimap
    this.markEnemyOnMinimap(enemy);

    // Set fight possible and internally load enemy to cache
    this.markFightPossibleAndLoadEnemy(enemy);
  }

  markEnemyOnMinimap(enemy: Enemy) {
    this.floorEnemies.map((e: Enemy) => {
      if (e.id === enemy.id) {
        e.visibleOnMinimap = true;
      }
      return e;
    });
  }

  markFightPossibleAndLoadEnemy(enemy: Enemy) {
    this.possibleFight.next(true);
    this.activeFightEnemy.next(enemy);
  }

  markFightFleetAndUnloadEnemy() {
    this.possibleFight.next(false);
    this.activeFightEnemy.next(null);
  }

  isFightEntitiesPresent() {
    return (
      this.activeFightEntitiesRemains !== undefined
      && this.activeFightEntitiesRemains.enemy !== undefined
      && this.activeFightEntitiesRemains.enemy.blocked !== undefined
      && this.activeFightEntitiesRemains.enemy.hitpoints !== undefined
      && this.activeFightEntitiesRemains.enemy.dealt !== undefined
      && this.activeFightEntitiesRemains.enemy.mobType !== undefined
      && this.activeFightEntitiesRemains.player !== undefined
      && this.activeFightEntitiesRemains.player.blocked !== undefined
      && this.activeFightEntitiesRemains.player.hitpoints !== undefined
      && this.activeFightEntitiesRemains.player.dealt !== undefined
    );

  }

  getActiveFightEntitiesRemainsFor(remainsFor: 'player'|'enemy'): FightRemainsEnemy|FightRemainsPlayer {
    return this.activeFightEntitiesRemains[remainsFor];
  }

  getFightResult() {
    return this.activeFightEntitiesRemains.result;
  }

  getActiveFightEnemyRemainsId(): number {
    return this.activeFightEntitiesRemains.enemy.id || 0;
  }

  getEntityAttack(entityStatistics: EntityStatistics, gear: Gear[] = []): number {
    let affection = 0;
    if (gear.length > 0) {
      gear.forEach((g: Gear) => {
        if (g.affectWhat === 'strength') {
          affection += affection + g.power;
        }
      });
    }
    return Math.round(
      (
           (
             (
               entityStatistics.strength + affection
             ) + (
               entityStatistics.intelligence / 100
             )
           ) + (
             entityStatistics.agility / 50
           )
       ) * (
         entityStatistics.strength / 5
       )
    );
  }

  getEntityDefense(entityStatistics: EntityStatistics, gear: Gear[] = []): number {
    const modificator = 7;
    let affection = 0;
    if (gear.length > 0) {
      gear.forEach((g: Gear) => {
        if (g.affectWhat === 'defence') {
          affection += affection + g.power;
        }
      });
    }
    return Math.round(
      (
        (
          (
            (
              entityStatistics.defence + affection
            ) * (
              (
                (
                  entityStatistics.strength / entityStatistics.agility
                ) / modificator
              ) + (
                entityStatistics.intelligence / modificator
              )
            )
          )
        )
      )
    );
  }

  getEntityRemainingHitpoints(forWho: 'enemy'|'player'): number {
    if (forWho === 'player' && this.activeFightEntitiesRemains?.player?.hitpoints >= 0) {
      return this.activeFightEntitiesRemains.player.hitpoints;
    } else if (forWho === 'enemy' && this.activeFightEntitiesRemains?.enemy?.hitpoints >= 0) {
      return this.activeFightEntitiesRemains.enemy.hitpoints;
    }
    return 0;
  }

  getEntityHitpoints(entityStatistics: EntityStatistics, gear: Gear[] = []): number {
    let affection = 0;
    if (gear.length > 0) {
      gear.forEach((g: Gear) => {
        if (g.affectWhat === 'hitpoints') {
          affection += affection + g.power;
        }
      });
    }
    return Math.round(
      (
        (
          entityStatistics.vitality * entityStatistics.strength
        ) * (
          (
            entityStatistics.intelligence / 1000
          ) + 1
        )
      ) + affection
    );
  }

  getEntitySpeed(entityStatistics: EntityStatistics): number {
    return Math.floor(
      (
        entityStatistics.agility - (
          (
            entityStatistics.strength + entityStatistics.defence
          ) / entityStatistics.intelligence
        )
      )
    );
  }

  revealRoomOnPosition(roomIndex: number) {
    this.rooms = this.rooms.map((r) => {
      if (r.roomIndex === roomIndex && !r.revealed) {
        r.revealed = true;
      }
      return r;
    });
  }

  getActiveRoom(): Room {
    return this.rooms.filter((room: Room) => room.room_id === this.player.getValue().position)[0];
  }

  increaseFreeAbilityPoints(by: number = 1) {
    if (this.tempFreeAbilityPoints?.getValue() !== null) {
      this.tempFreeAbilityPoints?.next(this.tempFreeAbilityPoints?.getValue() + by);
    }
  }

  decreaseFreeAbilityPoints(by: number = 1) {
    if (this.tempFreeAbilityPoints?.getValue() !== null) {
      this.tempFreeAbilityPoints?.next(this.tempFreeAbilityPoints?.getValue() - by);
    }
  }

  isFreeAbilityPoint(): boolean {
    return (this.tempFreeAbilityPoints?.getValue() || 0) > 0;
  }

  addPointToPlayerAbility(ability: EntityStatisticsType) {
    if (!this.newLevelAttributePointsDistribution[ability]) {
      this.newLevelAttributePointsDistribution[ability] = 0;
    }
    if (this.newLevelAttributePointsDistribution[ability] >= 0) {
      this.newLevelAttributePointsDistribution[ability] = this.newLevelAttributePointsDistribution[ability] + 1;
      this.decreaseFreeAbilityPoints();
    }
  }

  removePointToPlayerAbility(ability: EntityStatisticsType) {
    if (this.newLevelAttributePointsDistribution[ability] && this.newLevelAttributePointsDistribution[ability] - 1 >= 0) {
      this.newLevelAttributePointsDistribution[ability] = this.newLevelAttributePointsDistribution[ability] - 1;
      this.increaseFreeAbilityPoints();
    } else {
      this.newLevelAttributePointsDistribution[ability] = 0;
    }
  }

  getPlayerLevelProgressBarWidth(): number {
    return Math.ceil((this.player.getValue().actualXp / this.player.getValue().xpToNextLevel) * 100) || 0;
  };

  getPlayerRoomIdAfterInit() {
    this.rooms.filter((room: Room) => room.room_id === this.player.getValue().position)
  }

  setFightItems(item: Item) {
    this.fightItems.push(item);
  }

  getFightItems() {
    return this.fightItems;
  }

  useFightItem(item: Item) {
    // remove item from fight items
    this.fightItems = this.fightItems.filter(this.isItemNotSameAs(item));
    this.attackEnemy(item);
  }

  isItemNotSameAs(item: Item): (i: Item) => boolean {
    return (i: Item) => (
      i.item_id !== item.item_id
      || i.name !== item.name
      || i.affectWho !== item.affectWho
      || i.power !== item.power
      || i.duration !== item.duration
      || i.affectWhat !== item.affectWhat
      || i.affectHow !== item.affectHow
      || i.type !== item.type
      || i.visibilityOnMinimap !== item.visibilityOnMinimap
    );
  }

  showResetFloorButton() {
    return (
      (
        !this.isFightPossible() && !this.isFightActivated() && !this.isFightEntitiesPresent()
      )
      && this.floorEnemies.filter((e: Enemy) => e.visibleOnMinimap).length === this.floorEnemies.length
      && this.rooms.filter((r: Room) => r.revealed).length >= this.rooms.length - 5
    );
  }

  private setPlayerNewEntityStatistics(entityStatistics: EntityStatistics) {
    let updated = this.player.getValue();
    updated.freeAbilityPoints = 0;
    Object.keys(entityStatistics).map((attribute: string) => {
      updated.entityStatistics[attribute as 'strength' | 'defence' | 'intelligence' | 'vitality' | 'agility'] = (
        updated.entityStatistics[attribute as 'strength' | 'defence' | 'intelligence' | 'vitality' | 'agility']
        +
        entityStatistics[attribute as 'strength' | 'defence' | 'intelligence' | 'vitality' | 'agility']
      );
    });
    updated = this.handleIncomingBooleansOFPlayer(updated);
    this.player.next(updated);
  }

  private setPlayerNewLevelAlreadySet() {
    let updated = this.player.getValue();
    updated.newLevel = false;
    updated = this.handleIncomingBooleansOFPlayer(updated);
    this.player.next(updated);
  }

  private clearNewLevelTempData() {
    this.newLevelAttributePointsDistribution = {} as any;
    delete this.tempFreeAbilityPoints;
  }

  private handleNewFreeAbilityPoints() {
    if (!this.tempFreeAbilityPoints && this.player.getValue().freeAbilityPoints > 0) {
      this.tempFreeAbilityPoints = new BehaviorSubject<number>(this.player.getValue().freeAbilityPoints)
    }
  }

  private handleIncomingBooleansOFPlayer(player: Player) {
    player.newLevel = ((player.newLevel).toString() === 'true');
    player.activeFight = ((player.activeFight).toString() === 'true');
    return player;
  }

  private handleIncomingBooleansOFRoom(rooms: Room[]) {
    rooms = rooms.map((room: Room) => {
      room.exitWest = (room.exitWest).toString() === 'true';
      room.exitEast = (room.exitEast).toString() === 'true';
      room.exitNorth = (room.exitNorth).toString() === 'true';
      room.exitSouth = (room.exitSouth).toString() === 'true';
      room.revealed = (room.revealed).toString() === 'true';
      return room;
    })
    return rooms;
  }

  private handleIncomingBooleansOFItem(items: Gear[]) {
    items = items.map((i: Gear) => {
      i.visibilityOnMinimap = (i.visibilityOnMinimap).toString() === 'true';
      if (i.wearing) {
        i.wearing = (i.wearing).toString() === 'true';
      }
      return i;
    });
    return items;
  }

  private handleIncomingBooleansOFFloorEnemies(enemies: Enemy[]) {
    enemies = enemies.map((i: Enemy) => {
      i.visibleOnMinimap = (i.visibleOnMinimap).toString() === 'true';
      return i;
    });
    return enemies;
  }

  private isAllExplored() {
    const state = (
      (
        !this.isFightPossible() && !this.isFightActivated() && !this.isFightEntitiesPresent() && !this.haveSomeNewItems()
      )
      && this.floorEnemies.filter((e: Enemy) => e.visibleOnMinimap).length === this.floorEnemies.length
      && this.rooms.filter((r: Room) => r.revealed).length === this.rooms.length
    );
    if (state) {
      this.allExplored.next(state);
    }
  }
}
