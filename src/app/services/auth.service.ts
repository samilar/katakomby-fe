import { ElementRef, Injectable, NgZone } from "@angular/core";
import { GOOGLE_CLIENT_ID } from "./mock.service";
import { gapi } from "../interfaces/gapi.interface";
import GoogleAuthBase = gapi.auth2.GoogleAuthBase;

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private loginElement!: ElementRef;
  private auth2: GoogleAuthBase = {} as GoogleAuthBase;

  constructor(
    private ngZone: NgZone
  ) {
  }

  setLoginButtonReference(loginElement?: ElementRef) {
    if (loginElement) {
      this.loginElement = loginElement;
      this.googleAuthSDK();
    }
  }

  getGapi() {
    return this.auth2;
  }

  connectLoginButton() {
    this.auth2.attachClickHandler(this.loginElement?.nativeElement, {},
      (googleAuthUser: any) => {
        /* Write Your Code Here */
      }, (error:any) => {
        alert(JSON.stringify(error, undefined, 2));
      });
  }

  googleAuthSDK() {

    (<any>window)['googleSDKLoaded'] = () => {
      (<any>window)['gapi'].load('auth2', () => {
        this.ngZone.run(() =>
          this.auth2 = (<any>window)['gapi'].auth2.init({
            client_id: GOOGLE_CLIENT_ID,
            cookiepolicy: 'single_host_origin',
            scope: 'profile email',
            plugin_name: 'streamy'
          })
        );

        this.auth2.isSignedIn.listen((response: any) => {
          this.ngZone.run(() => this.auth2);
        })
        this.connectLoginButton();
      });
    }

    (function(d, s, id){
      let js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement('script');
      js.id = id;
      js.src = "https://apis.google.com/js/platform.js?onload=googleSDKLoaded";
      fjs?.parentNode?.insertBefore(js, fjs);
    }(document, 'script', 'google-jssdk'));

  }

  logout() {
    this.auth2.signOut();
  }

}
