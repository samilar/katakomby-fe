import {
  AfterViewChecked,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from "@angular/core";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewChecked {
  @ViewChild('loginRef', {static: true }) set content(content: ElementRef) {
    if(content) { // initially setter gets called with undefined
      this.loginElement = content;
    }
  };
  private loginElement?: ElementRef;

  constructor(
    public authService: AuthService
  ) {
  }

  ngOnInit(): void {
  }

  ngAfterViewChecked() {
    this.authService.setLoginButtonReference(this.loginElement);
  }

}
