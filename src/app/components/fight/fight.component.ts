import { Component, ViewEncapsulation } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { Item } from "../../interfaces/item.interface";

@Component({
  selector: 'app-fight',
  templateUrl: './fight.component.html',
  styleUrls: ['./fight.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FightComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

  itemUsed(item: Item): void {
    this.playerAndEnemyService.useFightItem(item);
  }
}
