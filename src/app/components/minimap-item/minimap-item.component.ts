import { Component, Input } from '@angular/core';
import { Item } from "../../interfaces/item.interface";

@Component({
  selector: 'app-minimap-item',
  templateUrl: './minimap-item.component.html',
  styleUrls: ['./minimap-item.component.scss']
})
export class MinimapItemComponent {
  @Input() playerInRoom: boolean = false;
  @Input() items: Item[] = [];

  constructor() {
  }

}
