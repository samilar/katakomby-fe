import { Component, ViewEncapsulation } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";

@Component({
  selector: 'app-fight-results',
  templateUrl: './fight-results.component.html',
  styleUrls: ['./fight-results.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FightResultsComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

}
