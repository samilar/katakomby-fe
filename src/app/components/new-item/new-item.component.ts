import { Component } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.scss']
})
export class NewItemComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

}
