import { Component } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";

@Component({
  selector: 'app-minimap',
  templateUrl: './minimap.component.html',
  styleUrls: ['./minimap.component.scss']
})
export class MinimapComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

  getMinimapBackgroundClass() {
    return this.playerAndEnemyService.floor ? 'background-pack-' + this.playerAndEnemyService.floor.minimapBackground : '';
  }
}
