import { Component, Input } from '@angular/core';
import { Room } from "../../interfaces/room.interface";

@Component({
  selector: 'app-minimap-player',
  templateUrl: './minimap-player.component.html',
  styleUrls: ['./minimap-player.component.scss']
})
export class MinimapPlayerComponent {
  @Input() playerInRoom: boolean = false;
  @Input() room?: Room;
  title = 'Hráč ';
}
