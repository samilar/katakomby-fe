import { Component, Input } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { Room } from "../../interfaces/room.interface";

@Component({
  selector: 'app-minimap-room',
  templateUrl: './minimap-room.component.html',
  styleUrls: ['./minimap-room.component.scss']
})
export class MinimapRoomComponent  {
  @Input() room: Room = {} as Room;

  title = 'Místnost';

  // private totalRoomsCount: number = 0;

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
    // this.totalRoomsCount = playerAndEnemyService.rooms.length;
  }

  isPlayerInRoom(room: Room) {
    // console.log('isPlayerInRoom', this.playerAndEnemyService.player.getValue(), room);
    return this.playerAndEnemyService.player.getValue().position === room.room_id || false;
  }

  getTexturePackClass(): string {
    return this.room.texturePack ? ' texture-pack-' + this.room.texturePack : '';
  }

  getMinimapRoomBackground(room: Room): string {
    let roomTexture = 'texture-';
    if (
      !room.exitEast && !room.exitNorth && !room.exitWest && !room.exitSouth
    ) {
      roomTexture = roomTexture + '0';
    } else if (
      !room.exitEast && room.exitNorth && !room.exitWest && !room.exitSouth
    ) {
      roomTexture = roomTexture + '1';
    } else if (
      room.exitEast && !room.exitNorth && !room.exitWest && !room.exitSouth
    ) {
      roomTexture = roomTexture + '2';
    } else if (
      !room.exitEast && !room.exitNorth && !room.exitWest && room.exitSouth
    ) {
      roomTexture = roomTexture + '3';
    } else if (
      !room.exitEast && !room.exitNorth && room.exitWest && !room.exitSouth
    ) {
      roomTexture = roomTexture + '4';
    } else if (
      !room.exitEast && room.exitNorth && room.exitWest && !room.exitSouth
    ) {
      roomTexture = roomTexture + '5';
    } else if (
      room.exitEast && room.exitNorth && !room.exitWest && !room.exitSouth
    ) {
      roomTexture = roomTexture + '6';
    } else if (
      room.exitEast && !room.exitNorth && !room.exitWest && room.exitSouth
    ) {
      roomTexture = roomTexture + '7';
    } else if (
      !room.exitEast && !room.exitNorth && room.exitWest && room.exitSouth
    ) {
      roomTexture = roomTexture + '8';
    } else if (
      !room.exitEast && room.exitNorth && room.exitWest && room.exitSouth
    ) {
      roomTexture = roomTexture + '9';
    } else if (
      room.exitEast && room.exitNorth && room.exitWest && !room.exitSouth
    ) {
      roomTexture = roomTexture + '10';
    } else if (
      room.exitEast && room.exitNorth && !room.exitWest && room.exitSouth
    ) {
      roomTexture = roomTexture + '11';
    } else if (
      room.exitEast && !room.exitNorth && room.exitWest && room.exitSouth
    ) {
      roomTexture = roomTexture + '12';
    } else if (
      room.exitEast && room.exitNorth && room.exitWest && room.exitSouth
    ) {
      roomTexture = roomTexture + '13';
    } else if (
      !room.exitEast && room.exitNorth && !room.exitWest && room.exitSouth
    ) {
      roomTexture = roomTexture + '14';
    } else if (
      room.exitEast && !room.exitNorth && room.exitWest && !room.exitSouth
    ) {
      roomTexture = roomTexture + '15';
    }
    return roomTexture;
  }

}
