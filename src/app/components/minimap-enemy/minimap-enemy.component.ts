import { Component, Input, OnInit } from '@angular/core';
import { Enemy } from "../../interfaces/enemy.interface";

@Component({
  selector: 'app-minimap-enemy',
  templateUrl: './minimap-enemy.component.html',
  styleUrls: ['./minimap-enemy.component.scss']
})
export class MinimapEnemyComponent implements OnInit {
  @Input() enemy: Enemy = {} as Enemy;
  constructor() {
  }

  ngOnInit() {
    // console.log('enemy component: ', this.enemy);
  }
}
