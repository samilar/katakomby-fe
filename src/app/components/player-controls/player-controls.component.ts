import { Component } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { Gear } from "../../interfaces/gear.interface";
import { EntityStatisticsType } from "../../interfaces/types/entityStatisticsType.type";

@Component({
  selector: 'app-player-controls',
  templateUrl: './player-controls.component.html',
  styleUrls: ['./player-controls.component.scss']
})
export class PlayerControlsComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

  itemClickedInPlayerControls(item: Gear) {
    // console.log('itemClickedInPlayerControls', item, this);
    if (!this.playerAndEnemyService.isFightActivated()) {
      if (item.type === 'gear') {
        item.wearing = !item.wearing;
        this.playerAndEnemyService.useGear(item);
      }
    }
  }

  getGearTextureClass(gearSection: 1 | 2 | 3 | 4 | 5 | 6): string {
    return this.getGear().filter((g: Gear) => true === g.wearing && gearSection === g.gearSection)[0].texture || '';
  }

  hasHelmet(): boolean {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 1 === g.gearSection).length > 0;
  }

  getHelmet(): Gear {
    return this.getGear().filter((g: Gear) => true === g.wearing && 1 === g.gearSection)[0];
  }

  getHelmetTextureClass(): string {
    return this.getGearTextureClass(1);
  }

  hasSword(): boolean {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 2 === g.gearSection).length > 0;
  }

  getSword(): Gear {
    return this.getGear().filter((g: Gear) => true === g.wearing && 2 === g.gearSection)[0];
  }

  getSwordTextureClass(): string {
    return this.getGearTextureClass(2);
  }

  hasChestPlate(): boolean {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 3 === g.gearSection).length > 0;
  }

  getChestPlate(): Gear {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 3 === g.gearSection)[0];
  }

  getChestPlateTextureClass(): string {
    return this.getGearTextureClass(3);
  }

  hasShield(): boolean {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 4 === g.gearSection).length > 0;
  }

  getShield(): Gear {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 4 === g.gearSection)[0];
  }

  getShieldTextureClass(): string {
    return this.getGearTextureClass(4);
  }

  hasLeggings(): boolean {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 5 === g.gearSection).length > 0;
  }

  getLeggings(): Gear {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 5 === g.gearSection)[0];
  }

  getLeggingsTextureClass(): string {
    return this.getGearTextureClass(5);
  }

  hasBoots(): boolean {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 6 === g.gearSection).length > 0;
  }

  getBoots(): Gear {
    return this.getGear()?.filter((g: Gear) => true === g.wearing && 6 === g.gearSection)[0];
  }

  getBootsTextureClass(): string {
    return this.getGearTextureClass(6);
  }

  getGearEffectFor(statisticsFor: EntityStatisticsType | 'hitpoints') {
    let effectHowMuch = 0;
    this.getGear()?.forEach((g: Gear) => {
      if (g.affectWhat === statisticsFor) {
        effectHowMuch = effectHowMuch + g.power;
      }
    });
    return effectHowMuch;
  }

  private getGear(): Gear[] {
    return this.playerAndEnemyService.player.getValue().gear;
  }

}
