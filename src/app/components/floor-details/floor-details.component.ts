import { Component } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";

@Component({
  selector: 'app-floor-details',
  templateUrl: './floor-details.component.html',
  styleUrls: ['./floor-details.component.scss']
})

export class FloorDetailsComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

}
