import { Component, Input, NgZone } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { Gear } from "../../interfaces/gear.interface";

@Component({
  selector: 'app-gear-item',
  templateUrl: './gear-item.component.html',
  styleUrls: ['./gear-item.component.scss']
})
export class GearItemComponent {
  @Input() gear?: Gear;
  @Input() callback?: (gear: Gear) => void;

  constructor(
    private ngZone: NgZone,
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

  getGearTitle(gear: Gear) {
    return this.playerAndEnemyService.getItemOrGearTitle(gear);
  }

  gearClick(gear: Gear) {
    this.ngZone.run(() => {
      if (this.callback) {
        this.callback(gear);
      }
    });
  }

}
