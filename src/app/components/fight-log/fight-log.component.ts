import { Component, ViewEncapsulation } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";

@Component({
  selector: 'app-fight-log',
  templateUrl: './fight-log.component.html',
  styleUrls: ['./fight-log.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FightLogComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

}
