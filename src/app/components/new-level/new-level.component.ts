import { Component } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";

@Component({
  selector: 'app-new-level',
  templateUrl: './new-level.component.html',
  styleUrls: ['./new-level.component.scss']
})
export class NewLevelComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

  canSave(): boolean {
    return this.playerAndEnemyService.tempFreeAbilityPoints !== undefined && this.playerAndEnemyService.tempFreeAbilityPoints?.getValue() <= 0;
  }

}
