import { Component } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";

@Component({
  selector: 'app-movements-layer',
  templateUrl: './movements-layer.component.html',
  styleUrls: ['./movements-layer.component.scss']
})
export class MovementsLayerComponent {

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

}
