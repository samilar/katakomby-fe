import { Component, Input } from '@angular/core';
import { Item } from "../../interfaces/item.interface";
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { Gear } from "../../interfaces/gear.interface";

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent {
  @Input() inFightWindow: boolean = false;
  @Input() callback?: (item:Item) => void;


  show: 'items'|'gear' = 'items';

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

  getOnlyItems() {
    return this.playerAndEnemyService.player.getValue().items.filter((g: Gear) => g.type === 'item' );
  }

  getOnlyGear() {
    return this.playerAndEnemyService.player.getValue().items.filter((g: Gear) => g.type === 'gear' );
  }

}
