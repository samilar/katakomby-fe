import { Component, Input } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { EntityStatistics } from "../../interfaces/types/entityStatistics.type";
import { Gear } from "../../interfaces/gear.interface";

@Component({
  selector: 'app-fight-entity-box',
  templateUrl: './fight-entity-box.component.html',
  styleUrls: ['./fight-entity-box.component.scss']
})
export class FightEntityBoxComponent {
  @Input() forEntity?: 'player' | 'enemy';

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

  getEntity() {
    let entity: EntityStatistics = this.playerAndEnemyService.player.getValue().entityStatistics;
    if (this.forEntity === 'player') {
      entity = this.playerAndEnemyService.player.getValue().entityStatistics
    } else if (this.forEntity === 'enemy') {
      entity = this.playerAndEnemyService.getRoomEnemy(this.playerAndEnemyService.getActiveRoom()).entityStatistics
    }
    return entity;
  }

  getEntityAttack(): number {
    let gear: Gear[] = [];
    if (this.forEntity === 'player') {
      gear = this.playerAndEnemyService.player.getValue().gear;
    }
    return this.playerAndEnemyService.getEntityAttack(this.getEntity(), gear);
  }

  getEntityDefense(): number {
    let gear: Gear[] = [];
    if (this.forEntity === 'player') {
      gear = this.playerAndEnemyService.player.getValue().gear;
    }
    return this.playerAndEnemyService.getEntityDefense(this.getEntity(), gear);
  }

  getEntitySpeed(): number {
    return this.playerAndEnemyService.getEntitySpeed(this.getEntity());
  }

  getEntityHitpoints(): number {
    let gear: Gear[] = [];
    if (this.forEntity === 'player') {
      gear = this.playerAndEnemyService.player.getValue().gear;
    }
    return this.playerAndEnemyService.getEntityHitpoints(this.getEntity(), gear);
  }

  getEntityRemainingHitpoints(): number {
    if (this.forEntity) {
      return this.playerAndEnemyService.getEntityRemainingHitpoints(this.forEntity);
    }
    return 0;
  }

  getEntityHealthbarWidth(): number {
    if (this.forEntity) {
      let gear: Gear[] = [];
      if (this.forEntity === 'player') {
        gear = this.playerAndEnemyService.player.getValue().gear;
      }
      return Math.ceil(
        (
          this.playerAndEnemyService.getEntityRemainingHitpoints(this.forEntity)
          /
          this.playerAndEnemyService.getEntityHitpoints(this.getEntity(), gear)
        )
        * 100
      );
    }
    return 0;
  }

}
