import { Component, NgZone, OnDestroy, ViewEncapsulation } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { Item } from "../../interfaces/item.interface";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-fight-item-pick',
  templateUrl: './fight-item-pick.component.html',
  styleUrls: ['./fight-item-pick.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FightItemPickComponent implements OnDestroy {
  firstSelectedItem?: Item;
  secondSelectedItem?: Item;
  private reloadFightItemsFromFightItemPickComponentSubscription: Subscription;

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
    this.reloadFightItemsFromFightItemPickComponentSubscription = playerAndEnemyService.transferFightItemsFromFightItemPickComponent
      .subscribe((activeFight: boolean) => {
        if (activeFight && playerAndEnemyService.getFightItems().length <= 1) {
          if (this.firstSelectedItem && playerAndEnemyService.getFightItems().length === 0) {
            playerAndEnemyService.setFightItems(this.firstSelectedItem);
            delete this.firstSelectedItem;
          }
          if (this.secondSelectedItem && playerAndEnemyService.getFightItems().length === 1) {
            playerAndEnemyService.setFightItems(this.secondSelectedItem);
            delete this.secondSelectedItem;
          }
        }
      });
  }

  ngOnDestroy() {
    this.reloadFightItemsFromFightItemPickComponentSubscription.unsubscribe();
  }

  selectedItemClickedInFight(item: Item) {
    // vraceni itemu do inventare
    const updated = this.playerAndEnemyService.player.getValue();
    updated.items.push(item);
    this.playerAndEnemyService.player.next(updated);
    if (
      this.firstSelectedItem
      && this.firstSelectedItem.item_id === item.item_id
      && this.firstSelectedItem.name === item.name
      && this.firstSelectedItem.affectWho === item.affectWho
      && this.firstSelectedItem.power === item.power
      && this.firstSelectedItem.duration === item.duration
      && this.firstSelectedItem.affectWhat === item.affectWhat
      && this.firstSelectedItem.affectHow === item.affectHow
      && this.firstSelectedItem.type === item.type
      && this.firstSelectedItem.visibilityOnMinimap === item.visibilityOnMinimap
    ) {
      delete this.firstSelectedItem;
    } else if (
      this.secondSelectedItem
      && this.secondSelectedItem.item_id === item.item_id
      && this.secondSelectedItem.name === item.name
      && this.secondSelectedItem.affectWho === item.affectWho
      && this.secondSelectedItem.power === item.power
      && this.secondSelectedItem.duration === item.duration
      && this.secondSelectedItem.affectWhat === item.affectWhat
      && this.secondSelectedItem.affectHow === item.affectHow
      && this.secondSelectedItem.type === item.type
      && this.secondSelectedItem.visibilityOnMinimap === item.visibilityOnMinimap
    ) {
      delete this.secondSelectedItem;
    }
  }

  itemClickedInFight(item: Item) {
    if (!this.firstSelectedItem || !this.secondSelectedItem) {
      if (!this.firstSelectedItem) {
        this.firstSelectedItem = item;
        // odebrani itemu z inventare
        this.removeItemFromPlayerInventory(item);
      } else if (!this.secondSelectedItem) {
        this.secondSelectedItem = item;
        // odebrani itemu z inventare
        this.removeItemFromPlayerInventory(item);
      }
    }
  }

  removeItemFromPlayerInventory(item: Item) {
    const updated = this.playerAndEnemyService.player.getValue();
    updated.items = updated.items.filter(this.playerAndEnemyService.isItemNotSameAs(item));
    this.playerAndEnemyService.player.next(updated);
  }

}
