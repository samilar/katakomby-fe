import { Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { LoadingService } from "../../services/loading.service";

@Component({
  selector: 'app-floor',
  templateUrl: './floor.component.html',
  styleUrls: ['./floor.component.scss']
})

export class FloorComponent implements OnInit, OnDestroy {
  title = 'Patro';
  globalListenFunc?: Function;

  debug: boolean = false;

  constructor(
    public playerAndEnemyService: PlayerAndEnemyService,
    private renderer: Renderer2,
    private loadingService: LoadingService
  ) {
  }

  ngOnInit() {
    // After login application initialization starts here
    this.playerAndEnemyService.initApplication();
    this.globalListenFunc = this.renderer.listen('document', 'keydown', e => {
      // console.log('>>>>>>>>>>> keycode:', e.code);
      if (!this.loadingService.isLoading()) {
        switch (e.code) {
          case 'Digit1':
          case 'Digit2':
          case 'Digit3':
          case 'Digit4':
          case 'Digit5':
          case 'Digit6':
          case 'Digit7':
          case 'Digit8':
          case 'Digit9':
          case 'Digit0':
            let usedIndex = parseInt(e.code.replace('Digit', ''));
            if (0 === usedIndex) {
              usedIndex = 10;
            } else {
              usedIndex = usedIndex - 1;
            }
            if (
              this.playerAndEnemyService.isFightEntitiesPresent()
              && this.playerAndEnemyService.isFightActivated()
              && this.playerAndEnemyService.getFightItems().length > 0
              && this.playerAndEnemyService.getFightItems()[usedIndex]
            ) {
              this.playerAndEnemyService.useFightItem(this.playerAndEnemyService.getFightItems()[usedIndex])
            }
            break;
          case 'ArrowDown':
          case 'KeyS':
            if (
              !this.playerAndEnemyService.isFightPossible()
              && !this.playerAndEnemyService.isFightEntitiesPresent()
              && !this.playerAndEnemyService.isFightActivated()
              && !this.playerAndEnemyService.haveSomeNewItems()
            ) {
              this.playerAndEnemyService.go('south');
            }
            break;
          case 'ArrowUp':
          case 'KeyW':
            if (
              !this.playerAndEnemyService.isFightPossible()
              && !this.playerAndEnemyService.isFightEntitiesPresent()
              && !this.playerAndEnemyService.isFightActivated()
              && !this.playerAndEnemyService.haveSomeNewItems()
            ) {
              this.playerAndEnemyService.go('north');
            }
            break;
          case 'ArrowLeft':
          case 'KeyA':
            if (
              !this.playerAndEnemyService.isFightPossible()
              && !this.playerAndEnemyService.isFightEntitiesPresent()
              && !this.playerAndEnemyService.isFightActivated()
              && !this.playerAndEnemyService.haveSomeNewItems()
            ) {
              this.playerAndEnemyService.go('west');
            }
            break;
          case 'ArrowRight':
          case 'KeyD':
            if (
              !this.playerAndEnemyService.isFightPossible()
              && !this.playerAndEnemyService.isFightEntitiesPresent()
              && !this.playerAndEnemyService.isFightActivated()
              && !this.playerAndEnemyService.haveSomeNewItems()
            ) {
              this.playerAndEnemyService.go('east');
            }
            break;
          case 'Escape':
          case 'KeyQ':
            if (!this.playerAndEnemyService.isFightEntitiesPresent() && !this.playerAndEnemyService.isFightActivated() && this.playerAndEnemyService.isFightPossible()) {
              this.playerAndEnemyService.fleeFight();
            }
            break;
          case 'Space':
          case 'KeyE':
            if (this.playerAndEnemyService.haveSomeNewItems()
            ) {
              this.playerAndEnemyService.deleteNewItems();
            } else {
              if (this.playerAndEnemyService.hasRoomEnemy(this.playerAndEnemyService.getActiveRoom())) {
                if (this.playerAndEnemyService.isFightEntitiesPresent() && this.playerAndEnemyService.isFightActivated()) {
                  this.playerAndEnemyService.attackEnemy();
                } else if (this.playerAndEnemyService.isFightEntitiesPresent() && !this.playerAndEnemyService.isFightActivated()) {
                  this.playerAndEnemyService.endFight();
                } else if (
                  !this.playerAndEnemyService.isFightEntitiesPresent()
                  && !this.playerAndEnemyService.isFightActivated()
                  && this.playerAndEnemyService.canStartFight()
                  && !this.playerAndEnemyService.haveSomeNewItems()
                ) {
                  this.playerAndEnemyService.doFight();
                } else if (!this.playerAndEnemyService.canStartFight()) {
                  this.playerAndEnemyService.fleeFight();
                }
              }
            }
            break;
        }
      }
    });
  }

  ngOnDestroy() {
    // remove listeners
    if (this.globalListenFunc) {
      this.globalListenFunc();
    }
  }

}
