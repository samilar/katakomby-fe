import { Component, Input, NgZone } from '@angular/core';
import { Item } from "../../interfaces/item.interface";
import { PlayerAndEnemyService } from "../../services/player-and-enemy.service";
import { Gear } from "../../interfaces/gear.interface";

@Component({
  selector: 'app-inventory-item',
  templateUrl: './inventory-item.component.html',
  styleUrls: ['./inventory-item.component.scss']
})
export class InventoryItemComponent {
  @Input() item?: Gear;
  @Input() callback?: (item: Gear) => void;

  constructor(
    private ngZone: NgZone,
    public playerAndEnemyService: PlayerAndEnemyService
  ) {
  }

  getItemClass(item: Gear) {
    let itemClass = '';
    if (item.type === 'item') {
      if (item.affectWhat === 'hitpoints' && item.type === 'item' && item.affectHow === 'positive') {
        itemClass = 'hp-potion';
      }
    }
    if (item.type === 'gear') {
      itemClass = 'inventory-gear';
    }
    return itemClass;
  }

  getItemTitle(item: Gear) {
    return this.playerAndEnemyService.getItemOrGearTitle(item);
  }

  itemClick(item: Gear) {
    this.ngZone.run(() => {
      if (!this.playerAndEnemyService.isFightActivated()) {
        if (this.callback) {
          this.callback(item);
        }
      }
    });
  }

}
