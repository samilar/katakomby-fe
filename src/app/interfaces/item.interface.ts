import { EntityStatisticsType } from "./types/entityStatisticsType.type";

export interface Item {
  item_id: string;
  name: string;
  type: 'item'|'ability'|'gear';
  duration: number;
  power: number;

  affectWhat: EntityStatisticsType | 'hitpoints';
  affectHow: 'negative'|'positive';
  affectWho: 'player'|'enemy';

  visibilityOnMinimap: boolean
}
