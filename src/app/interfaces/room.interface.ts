import { Item } from "./item.interface";

export interface Room {
  room_id: string;
  roomIndex: number;
  col: number;
  row: number;
  exitWest: boolean;
  exitEast: boolean;
  exitNorth: boolean;
  exitSouth: boolean;
  revealed: boolean;
  texturePack: string;
  items: Item[]
}
