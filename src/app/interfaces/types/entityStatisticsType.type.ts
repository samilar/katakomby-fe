export type EntityStatisticsType = 'strength' | 'defence' | 'vitality' | 'agility' | 'intelligence';
