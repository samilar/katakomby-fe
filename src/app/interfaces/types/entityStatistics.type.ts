import { EntityStatisticsType } from "./entityStatisticsType.type";

export type EntityStatistics = {
  [key in EntityStatisticsType]: number;
} & {
  strength: number;
  defence: number;
  intelligence: number;
  vitality: number;
  agility: number;
  hitpoints?: number;
};
