import { MobType } from "./types/mobType.type";

export interface FightRemainsPlayer {
  hitpoints: number;
  blocked: number;
  dealt: number;
  xpGain: number;
  mobType?: MobType;
  id?: number;
  xpFromKill?: number;
}

export interface FightRemainsEnemy extends FightRemainsPlayer {
  mobType?: MobType;
  id?: number;
}


export interface FightRemains {
  player: FightRemainsPlayer;
  enemy: FightRemainsEnemy;
  result: string;
}
