import { Ability } from "./ability.interface";

export interface Gear extends Ability {
  gearSection?: 1 | 2 | 3 | 4 | 5 | 6;
  wearing?: boolean;
  texture?: string;
}
