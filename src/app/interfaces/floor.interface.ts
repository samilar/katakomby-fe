export interface Floor {
  floor: number;
  cols: number;
  rows: number;
  minimapBackground: string;
  themePack: string;
}
