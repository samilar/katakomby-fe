import { EntityStatistics } from "./types/entityStatistics.type";
import { MobType } from "./types/mobType.type";

export interface Enemy {
  id: number;
  position: string;
  name: string;
  mobType: MobType;
  visibleOnMinimap: boolean;
  xpFromKill: number;
  entityStatistics: EntityStatistics
}
