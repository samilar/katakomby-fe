import { EntityStatistics } from "./types/entityStatistics.type";
import { Item } from "./item.interface";
import { Ability } from "./ability.interface";
import { Gear } from "./gear.interface";

export interface Player {
  player_id: number;
  userId: string;
  position: string;
  positionBefore: string;
  actualXp: number;
  xpToNextLevel: number;
  level: number;
  freeAbilityPoints: number;
  newLevel: boolean;
  entityStatistics: EntityStatistics,
  gear: Gear[];
  items: Item[];
  abilities: Ability[];
  actualFloorNumber: number;
  losts: number;
  activeFight: boolean;
}
