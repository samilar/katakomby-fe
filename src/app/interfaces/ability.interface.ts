import { Item } from "./item.interface";

export interface Ability extends Item {
  level?: number;
  abilityType?: 'active'|'passive';
}
