ALTER TABLE `item_ability_prototypes`
  CHANGE `type` `type` set('item','ability','gear') COLLATE 'utf8_general_ci' NOT NULL AFTER `item_ability_prototypes_id`;

ALTER TABLE `item_ability_prototypes`
  ADD `gearSection` int(11) NULL;

ALTER TABLE `item_ability`
  CHANGE `type` `type` set('item','ability','gear') COLLATE 'utf8_general_ci' NOT NULL AFTER `name`,
  ADD `gearSection` int NULL;
